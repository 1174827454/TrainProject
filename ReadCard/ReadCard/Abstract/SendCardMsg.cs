﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard
{
    public abstract class SendCardMsg
    {
        protected const int FF = 0xFF;
        protected const int AA = 0xAA;
        private int commCount = 1;
        private byte[] GetCommCountByte()
        {
            byte[] returnVal = new byte[2];
            string hexStr = (commCount.ToString("X")).PadLeft(4, '0');
            returnVal[0] = Convert.ToByte(hexStr.Substring(0, 2), 16);
            returnVal[1] = Convert.ToByte(hexStr.Substring(3, 2), 16);
            CommCount();
            return returnVal;
        }

        protected abstract CardCmdType cmdType
        {
            get;
        }

        /// <summary>
        /// 组织成完整的控制命令
        /// </summary>
        /// <returns></returns>
        protected byte[] MontageByte()
        {
            byte[] commCountByte = GetCommCountByte();
            byte[] cmdInfo = CreateSendData();
            if (cmdInfo == null) cmdInfo = new byte[0];
            int length = 5 + cmdInfo.Length;
            byte[] returnVal = new byte[length];
            returnVal[0] = AA;
            returnVal[1] = FF;
            returnVal[2] = (byte)cmdType;
            for (int i = 0; i < cmdInfo.Length; i++)
            {
                returnVal[3 + i] = cmdInfo[i];
            }
            returnVal[returnVal.Length - 2] = commCountByte[0];
            returnVal[returnVal.Length - 1] = commCountByte[1];
            return returnVal;
        }

        private void CommCount()
        {
            if (commCount >= ushort.MaxValue)
            {
                commCount = -1;
            }
            commCount += 1;
        }
        protected abstract byte[] CreateSendData();

        public int NowCount { get { return commCount; } }
    }
}
