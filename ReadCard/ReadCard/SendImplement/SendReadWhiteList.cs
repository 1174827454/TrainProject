﻿using ReadCard.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.SendImplement
{
    public class SendReadWhiteList : SendCardMsg, IReadCardSender
    {
        private int _cardPosition;
        private SendReadWhiteList() { }
        public SendReadWhiteList(ushort cardPosition) 
        {
            _cardPosition = cardPosition;
        }

        protected override CardCmdType cmdType
        {
            get { return CardCmdType.ReadWhiteList; }
        }

        protected override byte[] CreateSendData()
        {
           return HelpCard.Instance.SetCardPositionByte(_cardPosition);
        }

        public byte[] SendMsg()
        {
            return this.MontageByte();
        }
    }
}
