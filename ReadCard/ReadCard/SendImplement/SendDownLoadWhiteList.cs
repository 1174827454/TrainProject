﻿using ReadCard.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.SendImplement
{
    /// <summary>
    /// 下发白名单
    /// </summary>
    class SendDownLoadWhiteList : SendCardMsg, IReadCardSender
    {
        private ushort _cardPosition;
        private string _cardId = string.Empty;
        private DateTime _startTime;
        private DateTime _endTime;
        private SendDownLoadWhiteList() { }
        protected override CardCmdType cmdType
        {
            get { return CardCmdType.DownLoadWhiteList; }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cardPosition">下发位置</param>
        /// <param name="Cardid">十六进制已倒序的卡ID</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        public SendDownLoadWhiteList(ushort cardPosition, string Cardid, DateTime startTime, DateTime endTime)
        {
            _cardPosition = cardPosition;
            _cardId = Cardid.PadLeft(8, '0');
            _startTime = startTime;
            _endTime = endTime;
        }


        protected override byte[] CreateSendData()
        {
            //00 01      58 95 DA F2   10 09 01 01 02    10 0C 01 03 04
            // 卡号位置     卡号         有效期开始         有效期结束
            byte[] cmdInfo = new byte[16];
            byte[] hexPos = HelpCard.Instance.SetCardPositionByte(_cardPosition);
            cmdInfo[0] = hexPos[0];
            cmdInfo[1] = hexPos[1];
            string temp = new string(_cardId.ToCharArray());
            //数组2-5位置
            for (int i = 0; i < temp.Length / 2; i++)
            {
                string byteStr = temp.Substring(0, 2);
                cmdInfo[i + 2] = Convert.ToByte(byteStr, 16);
                temp = temp.Remove(0, 2);
            }

            cmdInfo[6] = Convert.ToByte(_startTime.ToString("yy"));
            cmdInfo[7] = (byte)_startTime.Month;
            cmdInfo[8] = (byte)_startTime.Day;
            cmdInfo[9] = (byte)_startTime.Hour;
            cmdInfo[10] = (byte)_startTime.Minute;

            cmdInfo[11] = Convert.ToByte(_endTime.ToString("yy"));
            cmdInfo[12] = (byte)_endTime.Month;
            cmdInfo[13] = (byte)_endTime.Day;
            cmdInfo[14] = (byte)_endTime.Hour;
            cmdInfo[15] = (byte)_endTime.Minute;
            return cmdInfo;
        }

        public byte[] SendMsg()
        {
            return this.MontageByte();
        }
    }
}
