﻿using ReadCard.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.SendImplement
{
    public class SendDownLoadClock : SendCardMsg, IReadCardSender
    {
        private DateTime _nowDate = DateTime.Now;
        private SendDownLoadClock() { }
        public SendDownLoadClock(DateTime nowDate) 
        {
            _nowDate = nowDate;
        }

        protected override CardCmdType cmdType
        {
            get { return CardCmdType.DownLoadClock; }
        }

        protected override byte[] CreateSendData()
        {
            byte[] cmdInfo = new byte[6];
            cmdInfo[0] = Convert.ToByte(_nowDate.ToString("yy"));
            cmdInfo[1] = (byte)_nowDate.Month;
            cmdInfo[2] = (byte)_nowDate.Day;
            cmdInfo[3] = (byte)_nowDate.Hour;
            cmdInfo[4] = (byte)_nowDate.Minute;
            cmdInfo[5] = (byte)_nowDate.Second;
            return cmdInfo;
        }

        public byte[] SendMsg()
        {
            return this.MontageByte();
        }
    }
}
