﻿namespace ReadCard
{
    partial class ReadCard
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxListenPort = new System.Windows.Forms.TextBox();
            this.textBoxCardIp = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxCardPort = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonListen = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxCmd = new System.Windows.Forms.TextBox();
            this.buttonSend = new System.Windows.Forms.Button();
            this.textBoxMsgs = new System.Windows.Forms.TextBox();
            this.clear_screen = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "监听端口：";
            // 
            // textBoxListenPort
            // 
            this.textBoxListenPort.Location = new System.Drawing.Point(71, 23);
            this.textBoxListenPort.Name = "textBoxListenPort";
            this.textBoxListenPort.Size = new System.Drawing.Size(171, 21);
            this.textBoxListenPort.TabIndex = 1;
            this.textBoxListenPort.Text = "1234";
            // 
            // textBoxCardIp
            // 
            this.textBoxCardIp.Location = new System.Drawing.Point(77, 79);
            this.textBoxCardIp.Name = "textBoxCardIp";
            this.textBoxCardIp.Size = new System.Drawing.Size(177, 21);
            this.textBoxCardIp.TabIndex = 3;
            this.textBoxCardIp.Text = "192.168.0.101";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "读卡器IP：";
            // 
            // textBoxCardPort
            // 
            this.textBoxCardPort.Location = new System.Drawing.Point(77, 112);
            this.textBoxCardPort.Name = "textBoxCardPort";
            this.textBoxCardPort.Size = new System.Drawing.Size(177, 21);
            this.textBoxCardPort.TabIndex = 5;
            this.textBoxCardPort.Text = "1234";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "读卡器端口：";
            // 
            // buttonListen
            // 
            this.buttonListen.Location = new System.Drawing.Point(254, 20);
            this.buttonListen.Name = "buttonListen";
            this.buttonListen.Size = new System.Drawing.Size(75, 23);
            this.buttonListen.TabIndex = 8;
            this.buttonListen.Text = "监听";
            this.buttonListen.UseVisualStyleBackColor = true;
            this.buttonListen.Click += new System.EventHandler(this.buttonListen_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxListenPort);
            this.groupBox1.Controls.Add(this.buttonListen);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(374, 54);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "本机监听";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "命令串：";
            // 
            // textBoxCmd
            // 
            this.textBoxCmd.Location = new System.Drawing.Point(77, 147);
            this.textBoxCmd.Name = "textBoxCmd";
            this.textBoxCmd.Size = new System.Drawing.Size(177, 21);
            this.textBoxCmd.TabIndex = 11;
            // 
            // buttonSend
            // 
            this.buttonSend.Location = new System.Drawing.Point(266, 145);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(75, 23);
            this.buttonSend.TabIndex = 9;
            this.buttonSend.Text = "发送";
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // textBoxMsgs
            // 
            this.textBoxMsgs.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxMsgs.Location = new System.Drawing.Point(0, 174);
            this.textBoxMsgs.Multiline = true;
            this.textBoxMsgs.Name = "textBoxMsgs";
            this.textBoxMsgs.Size = new System.Drawing.Size(405, 88);
            this.textBoxMsgs.TabIndex = 12;
            // 
            // clear_screen
            // 
            this.clear_screen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.clear_screen.Location = new System.Drawing.Point(266, 110);
            this.clear_screen.Name = "clear_screen";
            this.clear_screen.Size = new System.Drawing.Size(75, 23);
            this.clear_screen.TabIndex = 13;
            this.clear_screen.Text = "清屏";
            this.clear_screen.UseVisualStyleBackColor = true;
            this.clear_screen.Click += new System.EventHandler(this.button1_Click);
            // 
            // ReadCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 262);
            this.Controls.Add(this.clear_screen);
            this.Controls.Add(this.textBoxMsgs);
            this.Controls.Add(this.buttonSend);
            this.Controls.Add(this.textBoxCmd);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBoxCardPort);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxCardIp);
            this.Controls.Add(this.label2);
            this.Name = "ReadCard";
            this.Text = "读卡器";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxListenPort;
        private System.Windows.Forms.TextBox textBoxCardIp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxCardPort;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonListen;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxCmd;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.TextBox textBoxMsgs;
        private System.Windows.Forms.Button clear_screen;
    }
}

