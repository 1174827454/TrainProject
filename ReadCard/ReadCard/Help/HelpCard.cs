﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard
{
    public class HelpCard
    {
        private static HelpCard _instance = null;
        public static HelpCard Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new HelpCard();
                return _instance;
            }
        }

        /// <summary>
        /// 获取十六进制卡号
        /// </summary>
        /// <param name="Id">十进制卡号</param>
        /// <returns>已反转的十六进制卡号</returns>
        public string CreateHexId(string Id)
        {
            if (Id == null || Id.Trim().Length == 0) return string.Empty;
            long cardid = Convert.ToInt64(Id);
            string hexCardid = (cardid.ToString("X2")).PadLeft(8, '0');
            return ReverseCardId(hexCardid);
        }
        /// <summary>
        /// 反转十六进制卡号
        /// </summary>
        /// <param name="hexCardid">未反转十六进制卡号</param>
        /// <returns>已反转的十六进制卡号</returns>
        public string ReverseCardId(string hexCardid)
        {
            string returnVal = string.Empty;
            while (hexCardid.Length > 0)
            {
                returnVal += hexCardid.Substring(hexCardid.Length - 2, 2);
                hexCardid = hexCardid.Remove(hexCardid.Length - 2);
            }
            return returnVal;
        }

        public byte[] SetCardPositionByte(int cardPosition)
        {
            byte[] cmdInfo = new byte[2];
            string positionStr = (cardPosition.ToString("X2")).PadLeft(4, '0');
            //数组0-1位置
            cmdInfo[0] = Convert.ToByte(positionStr.Substring(0, 2));
            cmdInfo[1] = Convert.ToByte(positionStr.Substring(2, 2));
            return cmdInfo;
        }
    }
    public enum GateStatus
    {
        Close = 0,
        Open = 1
    }

    public enum ClearType
    {
        WhiteList = 1,
        SwingCard = 2
    }
    public enum CardCmdType
    {
        CommTest = 1,
        DownLoadWhiteList = 2,
        ReadWhiteList = 3,
        DownLoadClock = 4,
        ReadClock = 5,
        GateControl = 6,
        ReadCardRecord = 7,
        ClearRecord = 8,
        CycGetDeviceId = 15,
        LastCmd = 238,
        DownLoadDeviceId = 239
    }
}
