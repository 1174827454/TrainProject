﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard
{
    public class HelpReadCardSender
    {
        private const int FF = 0xFF;
        private const int AA = 0xAA;
        private static HelpReadCardSender _instance = null;
        private int commCount = 1;
        private HelpReadCardSender()
        {

        }
        public int CommCount
        {
            get
            {
                return commCount;
            }
        }

        public void ResetCount()
        {
            commCount = 1;
        }

        public static HelpReadCardSender Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new HelpReadCardSender();
                return _instance;
            }
        }

        private byte[] GetCommCountByte()
        {
            byte[] returnVal = new byte[2];
            string hexStr = (commCount.ToString("X")).PadLeft(4, '0');
            returnVal[0] = Convert.ToByte(hexStr.Substring(0, 2), 16);
            returnVal[1] = Convert.ToByte(hexStr.Substring(3, 2), 16);
            commCount += 1;
            return returnVal;
        }
        #region 命令集合
        /// <summary>
        /// 测试命令
        /// </summary>
        /// <returns></returns>
        public byte[] GetTestCmd()
        {
            return MontageByte(AA, FF, CardCmdType.DownLoadWhiteList, new byte[0]);
        }
        /// <summary>
        /// 下载一条白名单 
        /// </summary>
        /// <param name="cardPosition">写入卡号位置</param>
        /// <param name="Cardid">卡号（16进制）</param>
        /// <param name="startTime">有效期开始时间</param>
        /// <param name="endTime">有效期结束时间</param>
        /// <returns></returns>
        public byte[] GetDownLoadWhiteListCmd(int cardPosition, string Cardid, DateTime startTime, DateTime endTime)
        {

            //00 01      58 95 DA F2   10 09 01 01 02    10 0C 01 03 04
            // 卡号位置     卡号         有效期开始         有效期结束
            Cardid = Cardid.PadLeft(8, '0');
            byte[] cmdInfo = new byte[16];
            SetCardpositionByte(cardPosition, cmdInfo);
            //数组2-5位置
            for (int i = 0; i < Cardid.Length / 2; i++)
            {
                string byteStr = Cardid.Substring(0, 2);
                cmdInfo[i + 2] = Convert.ToByte(byteStr, 16);
                Cardid.Remove(0, 2);
            }

            cmdInfo[6] = Convert.ToByte(startTime.ToString("yy"));
            cmdInfo[7] = (byte)startTime.Month;
            cmdInfo[8] = (byte)startTime.Day;
            cmdInfo[9] = (byte)startTime.Hour;
            cmdInfo[10] = (byte)startTime.Minute;

            cmdInfo[11] = Convert.ToByte(endTime.ToString("yy"));
            cmdInfo[12] = (byte)endTime.Month;
            cmdInfo[13] = (byte)endTime.Day;
            cmdInfo[14] = (byte)endTime.Hour;
            cmdInfo[15] = (byte)endTime.Minute;
            return MontageByte(AA, FF, CardCmdType.DownLoadWhiteList, cmdInfo);
        }



        /// <summary>
        /// 读取一条白名单
        /// </summary>
        /// <param name="cardPosition">读取卡号位置</param>
        /// <returns></returns>
        public byte[] ReadWhiteListCmd(int cardPosition)
        {
            byte[] cardByte = new byte[2];
            SetCardpositionByte(cardPosition, cardByte);
            return MontageByte(AA, FF, CardCmdType.ReadWhiteList, new byte[] { (byte)cardPosition });
        }
        /// <summary>
        /// 下载时钟
        /// </summary>
        /// <param name="downLoadTime"></param>
        /// <returns></returns>
        public byte[] DownLoadClockCmd(DateTime downLoadTime)
        {
            byte[] cmdInfo = new byte[6];
            cmdInfo[0] = Convert.ToByte(downLoadTime.ToString("yy"));
            cmdInfo[1] = (byte)downLoadTime.Month;
            cmdInfo[2] = (byte)downLoadTime.Day;
            cmdInfo[3] = (byte)downLoadTime.Hour;
            cmdInfo[4] = (byte)downLoadTime.Minute;
            cmdInfo[5] = (byte)downLoadTime.Second;
            return MontageByte(AA, FF, CardCmdType.DownLoadClock, cmdInfo);
        }
        /// <summary>
        /// 读取时钟
        /// </summary>
        /// <returns></returns>
        public byte[] ReadClockCmd()
        {
            return MontageByte(AA, FF, CardCmdType.ReadClock, new byte[0]);
        }

        /// <summary>
        /// 控制开关门
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public byte[] GateControlCmd(GateStatus status)
        {
            return MontageByte(AA, FF, CardCmdType.GateControl, new byte[] { (byte)status });
        }
        /// <summary>
        /// 读取刷卡记录
        /// </summary>
        /// <param name="cardPosition"></param>
        /// <returns></returns>
        public byte[] ReadCardRecordCmd(int cardPosition)
        {
            byte[] byteCmd = new byte[2];
            SetCardpositionByte(cardPosition, byteCmd);
            return MontageByte(AA, FF, CardCmdType.ReadCardRecord, byteCmd);
        }

        /// <summary>
        /// 清除白名单，刷卡记录
        /// </summary>
        /// <param name="clearType">清除类型</param>
        /// <returns></returns>
        public byte[] ClearRecordCmd(ClearType clearType)
        {
            return MontageByte(AA, FF, CardCmdType.ClearRecord, new byte[] { (byte)clearType });
        }

        /// <summary>
        /// 最后一条命令查询
        /// </summary>
        /// <returns></returns>
        public byte[] GetLastCmd()
        {
            return MontageByte(AA, FF, CardCmdType.LastCmd, new byte[0] { });
        }

        /// <summary>
        /// 下载设备ID
        /// </summary>
        /// <param name="DeviceId">十六进制ID</param>
        /// <returns></returns>
        public byte[] DownLoadDeviceIdCmd(string DeviceId)
        {
            DeviceId = DeviceId.PadLeft(8, '0');
            byte[] bytecmd = new byte[9];
            byte xor = 0;
            int position = 0;
            for (int i = 0; i < DeviceId.Length; i += 2)
            {
                byte temp = Convert.ToByte(DeviceId.Substring(i, 2));
                bytecmd[position] = temp;
                bytecmd[position + 4] = ((byte)~temp); //取反码
                xor ^= temp; //异或的结果
                position += 1;
            }
            bytecmd[bytecmd.Length - 1] = xor;
            return MontageByte(AA, FF, CardCmdType.DownLoadDeviceId, bytecmd);
        }
        #endregion

        private static void SetCardpositionByte(int cardPosition, byte[] cmdInfo)
        {
            string positionStr = (cardPosition.ToString("X2")).PadLeft(4, '0');
            //数组0-1位置
            cmdInfo[0] = Convert.ToByte(positionStr.Substring(0, 2));
            cmdInfo[1] = Convert.ToByte(positionStr.Substring(2, 2));
        }
        //组织控制命令
        private byte[] MontageByte(byte firstByte, byte secondByte, CardCmdType cType, byte[] cmdInfo)
        {
            byte[] commCountByte = GetCommCountByte();
            int length = 5 + cmdInfo.Length;
            byte[] returnVal = new byte[length];
            returnVal[0] = firstByte;
            returnVal[1] = secondByte;
            returnVal[2] = (byte)cType;
            for (int i = 0; i < cmdInfo.Length; i++)
            {
                returnVal[2 + i] = cmdInfo[i];
            }
            returnVal[returnVal.Length - 2] = commCountByte[0];
            returnVal[returnVal.Length - 1] = commCountByte[1];
            return returnVal;
        }
        /// <summary>
        /// 获取十六进制卡号
        /// </summary>
        /// <param name="Id">十进制卡号</param>
        /// <returns>已反转的十六进制卡号</returns>
        public string CreateHexId(string Id)
        {
            if (Id == null || Id.Trim().Length == 0) return string.Empty;
            long cardid = Convert.ToInt64(Id);
            string hexCardid = (cardid.ToString("X2")).PadLeft(8, '0');
            return ReverseCardId(hexCardid);
        }
        /// <summary>
        /// 反转十六进制卡号
        /// </summary>
        /// <param name="hexCardid">未反转十六进制卡号</param>
        /// <returns>已反转的十六进制卡号</returns>
        public string ReverseCardId(string hexCardid)
        {
            string returnVal = string.Empty;
            while (hexCardid.Length > 0)
            {
                returnVal += hexCardid.Substring(hexCardid.Length - 2, 2);
                hexCardid = hexCardid.Remove(hexCardid.Length - 2);
            }
            return returnVal;
        }
    }
}
