﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard
{
    public class HelpUdpListen
    {
        private UdpClient udpclient;
        private byte[] _reciveBytes;
        public delegate void ListenReciveBytes(byte[] reciveBytes);
        public event ListenReciveBytes listenReciveBytes;
        private IAsyncResult _async;
        public HelpUdpListen(int listenport)
        {
            udpclient = new UdpClient(listenport);     
        }

        public void StartListen()
        {
            AsyncRecive();
        }

        private void AsyncRecive()
        {
           _async = udpclient.BeginReceive(new AsyncCallback(CallBackMethod),null);
        }

        private void CallBackMethod(IAsyncResult result)
        {
            if (result.IsCompleted)
            {
                StopRecive();
                AsyncRecive();

            }
        }

        private void StopRecive()
        {
            IPEndPoint endPoint = null;
            _reciveBytes = udpclient.EndReceive(_async, ref endPoint);
            if (listenReciveBytes != null) listenReciveBytes(_reciveBytes);
        }

        public bool SendMessage(string Ip,int port,byte[] sendBytes)
        {
            try
            {
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(Ip), Convert.ToInt32(port));
                udpclient.Send(sendBytes, sendBytes.Length, remoteEP);
                return true;
            }
            catch (Exception)
            {          
                throw;
            }          
        }
    }
}
